# Como una ruta carga una vista

# Rutas

## Ubicación Rutas

En el folder rutas se encuentra el archivo con el nombre "web.php" es donde se escriben las rutas

## Como escribir una ruta

Para escribir una ruta se necesita seguir la siguiente sintaxis

Se reciben dos parametros el nombre de la ruta y el nombre de la vista que deseamos que retorne la ruta
```php
    Route::get('nombre de la ruta', function () { 
            return view('nombre de la vista');
        });

```
    En este caso al escribir "/" en el navegador nos va a redireccionar a la ruta con el nombre welcome
```php
    Route::get('/', function () { 
        return view('welcome');
    });
```
## Error 404
En caso de que no exista la ruta a la hora de ingresarla en el navegador nos mostrara un error

# Vistas

## Ubicación Vistas

En la carpeta resources se encuentra una carpeta con el nombre "views" en dicha carpeta se guardaran las vistas que creadas

## Como crear una vista

Para crear una vista se crea un archivo con la extensión blade.php y dentro del archivo escribimos nuestro html

    nombredelavista.blade.php 
    welcome.balde.php



