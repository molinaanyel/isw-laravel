
# Eloquent y el Active Record Patern

Eloquent nos permite interactuar con la base de datos comunicandose a travez del Active Record Patern 


## Tinker

Para hacer uso de Eloquent lo hacemos por medio de Tinker que es una consola donde se ejecutan los comandos de Eloqueten

Para activar tinker ingresamos el siguiente comando en nuestra consola:

```bash
    php artisan tinker
```

## Creación de un registro

Con la siguiente sintaxis creamos un registro en la base de datos

```bash
    $user = new App\ModelsApp\NombreDelModelo
    $user = new NombreModelo()

```

Le asignamos un valor a cada propiedad 

```bash
    $user->nombrepropiedad = "valor"

```

Para guardar el registro en la base de datos usamos la función de eloquent `save()`

```bash
    $user->save()

```

También podemos hacer uso de los métodos creados en nuestros modelos

```bash
    NombreModelo::NombreMetodo()
```