# Activate the comment form

En esta sección se activará el formulario de comentarios que se diseñó en la sección anterior para ello dentro del file `` web.php `` se debe agregar la entrada

```php
Route::post('posts/{post:slug}/comments');
 ```

Para asegurarnos que los comentarios se asignen a una lista específica, estas rutas siempre se deben mantener lo más cortas y ordenadas posibles.

Luego se puede crear un nuevo controlador dentro de `` PostCommentController.php `` 

```php
function store(Post $post) {
    $post->comments()->create([
        'user_id' => auth()->id(),
        'body' => request('body'),
    ])
}
 ```

 Dentro del formulario se debe actualizar la acción.
