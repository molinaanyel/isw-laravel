# Usando seeders para generar datos

Los seeders es una funcionalidad de Laravel con la cuál podemos generar datos de prueba sin tener que insertar los datos manualmente.


## Generando datos en la base de datos

Nos posicionamos en la carpeta seeders y editamos la función run
Mediante el método factory() que recibe como parámetro el número de registos que deseamos generar.

Creamos registros para las categorías y un usuario

Asocíamos un usuario a los post mediante la siguiente sintaxis:
`'user_id'=>$user->id`



```php
public function run()
    {
        
       $user = User::factory()->create();
       $family= Category::create(['name'=>'Family', 'slug'=>'family']);
       $personal= Category::create(['name'=>'Personal', 'slug'=>'personal']);
       Post::create(['user_id'=>$user->id, 'category_id'=>$family->id,'title'=> 'My Family Post', 'slug'=> 'my-first-post', 'excerpt'=>'Lorem ipsum dolar sit amet', 'body'=>'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique quibusdam deserunt provident nesciunt esse cupiditate alias, cumque sapiente pariatur odit nisi architecto maxime fugiat doloribus saepe, tenetur quia tempore asperiores']);
       Post::create(['user_id'=>$user->id, 'category_id'=>$personal->id,'title'=> 'My Personal Post', 'slug'=> 'my-second-post', 'excerpt'=>'Lorem ipsum dolar sit amet', 'body'=>'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique quibusdam deserunt provident nesciunt esse cupiditate alias, cumque sapiente pariatur odit nisi architecto maxime fugiat doloribus saepe, tenetur quia tempore asperiores']);

    }

```
Para ejecutar ese método corremos el siguiente comando en nuestra consola 

```bash
    php artisan db:seed
```


## Creación de una relación entre Post y Usuarios

Cada post va a tener su respectivo autor es por esto que vamos a crear una relación entre el Post y el modelo Usuario que representa el autor

Agregamos una llave foreana en la tabla Post, la llave hace referncia al id del usuario autor del post `$table->foreignId('category_id');`


```php
public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('category_id');
            $table->string('slug')->unique();
            $table->string('titld);
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
        });
    }

```
También agregamos una relación `belongsTo` ya que el post solo puede tener un autor

```php
    public function User()
    {
            return $this->belongsTo(User:class);
    }

```

En el modelo User agregamos una relación `hasMany` ya que un usuario puede tener muchos posts

```php
public function posts()
    {
        return $this->hasMany(Post::class);
    }
```
## Método truncate

Este método permite borrar todos los registros de una tabla en la base de datos.


## Mostrando el autor de los posts

Modificamos las vistas para mostrar el nombre del autor del post agregamos  `<a href="#">{{$post->user->name}}</a>` dentro de la etiqueta `<p>`