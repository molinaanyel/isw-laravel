# Como extraer un componente blade

A Continuación vamos separar el  dropdown que nos va mostrar las categorías en varios componentes


## Creación Dropdown

Copiamos el código del dropdown y lo pegamos un un nuevo componente:

Declaramos un evento click y una propiedad llamada trigger, cuando se dispare el evento click el contenido dentro de la propiedad trigger se va  mostrar

```html
    @props(['trigger'])

    <div x-data="{ show: false }" @click.away="show=false">
        <div @click="show = ! show">
            {{ $trigger }}
        </div>


        <div x-show="show" class="py-2 absolute bg-gray-100 w-full mt-2 rounded-xl z-50" style="display: none;">
            {{ $slot }}
        </div>
    </div>

```

## Creación Dropdown Item

Creamos un componente para los items del  dropdown 

Declaramos una propiedad llamada active para cambiar el color a azul cuando un item está siendo seleccionado

```html
@props(['active' => false])

  @php
      $classes = 'block text-left px-3 text-sm leading-6 hover:bg-blue-500
     focus:bg-blue-500 hover:text-white focus:text-white ';

      if ($active) {
          $classes .= 'bg-blue-400 text-white';
      }
  @endphp


  <a {{ $attributes->merge(['class' => $classes]) }}>
      {{ $slot }}
  </a>

```



## Modificación Header

Modificamos el header agregando el siguiente contenido:

Agregamos el componente dropdown y dropdown-item en la cabecera

Mediante el foreach recorremos la lista de categorías y se crea dinámicamente los items

Validamos cual item está activo mediante el nombre de la ruta  ,`<x-dropdown-item href="/" :active="request()->routeIs('home')">`



```html
<header class="max-w-xl mx-auto mt-20 text-center">
    <h1 class="text-4xl">
        Latest <span class="text-blue-500">Laravel From Scratch</span> News
    </h1>

    <h2 class="inline-flex mt-2">By Lary Laracore <img src="/images/lary-head.svg" alt="Head of Lary the mascot"></h2>

    <p class="text-sm mt-14">
        Another year. Another update. We're refreshing the popular Laravel series with new content.
        I'm going to keep you guys up to speed with what's going on!
    </p>

    <div class="space-y-2 lg:space-y-0 lg:space-x-4 mt-8">
        <!--  Category -->
        <div class="relative lg:inline-flex bg-gray-100 rounded-xl">
            <x-dropdown>

                <x-slot name="trigger">
                    <button
                        class="flex-1 appearance-none bg-transparent py-2 pl-3 pr-9
                    text-sm font-semibold w-full lg:w-32 text-left inline-flex">

                        {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}

                        <x-icon name="down-arrow" class="absolute pointer-events-none" style="right: 12px;" />
                    </button>
                </x-slot>

                <x-dropdown-item href="/" :active="request()->routeIs('home')">
                    All
                </x-dropdown-item>

                @foreach ($categories as $category)
                    <x-dropdown-item href="/categories/{{ $category->slug }}" :active='request()->is("categories/{$category->slug}")'>
                        {{ ucwords($category->name) }} </x-dropdown-item>
                @endforeach
            </x-dropdown>
        </div>

        <!-- Other Filters -->
        <div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl">
            <select class="flex-1 appearance-none bg-transparent py-2 pl-3 pr-9 text-sm font-semibold">
                <option value="category" disabled selected>Other Filters
                </option>

                <option value="foo">Foo
                </option>
                <option value="bar">Bar
                </option>
            </select>

            <svg class="transform -rotate-90 absolute pointer-events-none" style="right: 12px;" width="22"
                height="22" viewBox="0 0 22 22">
                <g fill="none" fill-rule="evenodd">
                    <path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M21 1v20.16H.84V1z">
                    </path>
                    <path fill="#222" d="M13.854 7.224l-3.847 3.856 3.847 3.856-1.184 1.184-5.04-5.04 5.04-5.04z">
                    </path>
                </g>
            </svg>
        </div>

        <!-- Search -->
        <div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl px-3 py-2">
            <form method="GET" action="#">
                <input type="text" name="search" placeholder="Find something"
                    class="bg-transparent placeholder-black font-semibold text-sm">
            </form>
        </div>
    </div>
</header>
```

## Modificación ruta

Creamos un nombre para una ruta de la siguiente manera

Mediante la sintaxis `name("nombreruta")`

```php
Route::get('categories/{category:slug}',function (Category $category) {
    return view('posts', ['posts' => $category->posts,
    'currentCategory' => $category,
    'categories'=> Category::all()]);
})->name('category');
```






