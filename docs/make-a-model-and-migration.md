# Creación de un Modelo y una Migración

A continuación los pasos para crear un modelo eloquent y una migración


## Creación de la migración

En la máquina virtual nos posicionamos en el directorio `cd /vagrant/sites/lfts.isw811.xyz/,` y ejecutamos el siguiente comando

La migración nos crea una tabla la cual podemos agregar las columnas que se necesiten 


```bash
    php artisan make:migration nombredelamigración

```

Modificamos la tabla post agregando los campos title, excerpt, body y published_at

```php
  public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('excerpt');
            $table->text('body');
            $table->timestamps();
            $table->timestamps('published_at')->nullable()
        });
    }
```

## Creación del Modelo

En la máquina virtual nos posicionamos en el directorio `cd /vagrant/sites/lfts.isw811.xyz/,` y ejecutamos el siguiente comando

Por convención el nombre del modelo es el nombre en singular de la tabla


```bash
    php artisan make:migration nombredelmodelo

```

## Creación de registros en la base de datos

Activamos tinker con el siguiente comando:


```bash
    php artisan tinker

```

```bash
    $post = new App\Models\Post
    $post->excerpt = 'Lorem ipsum dolar sit amet.'
    $post->title = "My First Post"
    $post->body ="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique quibusdam deserunt provident nesciunt esse cupiditate alias, cumque sapiente pariatur odit nisi architecto maxime fugiat doloribus saepe, tenetur quia tempore asperiores"
```

