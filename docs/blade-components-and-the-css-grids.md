# Componentes de Blade y Css grid

A continuación se va hacer uso de los componentes de blade para darle dinamismo una página


## Usando Propiedades

Declaramos una propieadad en el componente post-featured-card `@props(["post"])` y reemplazamos los datos estaticos del post por datos dinámicos mediante el uso de propiedades y repetimos el mismo proceso con el componente post-card


post-featured-card:
```html
@props(["post"])
    <article class="transition-colors duration-300 hover:bg-gray-100 border border-black border-opacity-0 hover:border-opacity-5 rounded-xl">
                    <div class="py-6 px-5 lg:flex">
                        <div class="flex-1 lg:mr-8">
                            <img src="./images/illustration-1.png" alt="Blog Post illustration" class="rounded-xl">
                        </div>

                        <div class="flex-1 flex flex-col justify-between">
                            <header class="mt-8 lg:mt-0">
                                <div class="space-x-2">
                                    <a href="/categories/{{ $post->category->slug }}"
                                    class="px-3 py-1 border border-blue-300 rounded-full text-blue-300 text-xs uppercase font-semibold"
                                    style="font-size: 10px">{{ $post->category->name }}</a>
                                </div>

                                <div class="mt-4">
                                    <a href="/posts/{{ $post->slug }}">
                                    
                                            {{ $post->title }}
                                         
                                    </a>
                                   
                                    <span class="mt-2 block text-gray-400 text-xs">
                                            Published <time>{{ $post->created_at->diffForHumans() }}</time>
                                        </span>
                                </div>
                            </header>

                            <div class="text-sm mt-2">
                                <p>
                                    {{ $post->excerpt}}
                                </p>

                            
                            </div>

                            <footer class="flex justify-between items-center mt-8">
                                <div class="flex items-center text-sm">
                                    <img src="./images/lary-avatar.svg" alt="Lary avatar">
                                    <div class="ml-3">
                                        <h5 class="font-bold">{{ $post->author->name }}</h5>
                                        <h6>Mascot at Laracasts</h6>
                                    </div>
                                </div>

                                <div class="hidden lg:block">
                                    <a href="/posts/{{ $post->slug }}"
                                    class="transition-colors duration-300 text-xs font-semibold bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8"
                                    >Read More</a>
                                </div>
                            </footer>
                        </div>
                    </div>
    </article>
```
 post-card:

 Extraemos los atributos que nos envien mediante `$attributes->merge`

  ```html
 @props(["post"])
<article
              {{ $attributes->merge(["class" => "transition-colors duration-300 hover:bg-gray-100 border border-black border-opacity-0 hover:border-opacity-5 rounded-xl"])}}>
                    <div class="py-6 px-5">
                        <div>
                            <img src="./images/illustration-3.png" alt="Blog Post illustration" class="rounded-xl">
                        </div>

                        <div class="mt-8 flex flex-col justify-between">
                            <header>
                                <div class="space-x-2">
                                    <a href="/categories/{{$post->category->slug}}"
                                       class="px-3 py-1 border border-blue-300 rounded-full text-blue-300 text-xs uppercase font-semibold"
                                       style="font-size: 10px">{{ $post->category->name }}</a>
            
                                </div>

                                <div class="mt-4">
                                    <h1 class="text-3xl">
                                        <a href="/posts/{{ $post->slug }}">
                                        {{ $post->title }}
                                        </a>
                                        
                                    </h1>

                                    <span class="mt-2 block text-gray-400 text-xs">
                                        Published <time>{{ $post->created_at->diffForHumans() }}</time>
                                    </span>
                                </div>
                            </header>

                            <div class="text-sm mt-4">
                                <p>
                                {{ $post->excerpt}}
                                </p>

                            </div>

                            <footer class="flex justify-between items-center mt-8">
                                <div class="flex items-center text-sm">
                                    <img src="./images/lary-avatar.svg" alt="Lary avatar">
                                    <div class="ml-3">
                                        <h5 class="font-bold">{{ $post->author->name }}</h5>
                                        <h6>Mascot at Laracasts</h6>
                                    </div>
                                </div>

                                <div>
                                    <a href="#"
                                       class="transition-colors duration-300 text-xs font-semibold bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8"
                                    >Read More</a>
                                </div>
                            </footer>
                        </div>
                    </div>
</article>

```

## Creación de Componentes

Creamos un nuevo componente que se va a encargar de ir creando los componentes dinamicamente y declaramos una propieadad `@props(["posts"])`

Validamos que la colección de Posts no esté vacía y en caso de ser así va ir creando todos los registros de los posts 

@props(['posts'])

<x-post-featured-card :post="$posts[0]" />

@if ($posts->count() > 1)
    <div class="lg:grid lg:grid-cols-6">
        @foreach ($posts->skip(1) as $post)
            <x-post-card :post="$post" class="{{ $loop->iteration < 3 ? 'col-span-3' : 'col-span-2' }}" />
        @endforeach

    </div>
@endif


## Modificación de las views 

Sustituimos el contenido de la vista Posts por el siguiente:

Validamos si la lista tiene contenido y hacemos uso del componente post-grid, en caso de estar vacía mostramos un párrafo con un mensaje de error

<x-layout>
@include ('_posts-header')

    <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">

        @if ($posts->count())
            <x-post-grid :posts="$posts" />
        @else
            <p class="text-center">No posts yet. Please check back later...</p>
        @endif
    </main>

</x-layout>



