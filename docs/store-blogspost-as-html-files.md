# Guardar un post como un archivo html
A continuación los pasos para almacenar archivos html con el fin de usarlos en una ruta dinámica


## Creación de los archivos html
Nos posicionamos en la carpeta resources `/resources` y creamos una carpeta con el nombre posts 

Una vez creada la carpeta posts creamos dentro de esta un archivo html para cada post

```html

   <h1><a href="/post"> My Firts Post</a></h1>  

   <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates aut rerum perferendis quod a molestiae pariatur, beatae reprehenderit consequatur, ex recusandae minus quis exercitationem at saepe officiis et. Modi!      
    </p> 
```

## Modificación de la ruta
Modificamos la ruta llamada post ubicada en el directorio `/routes/web.php`
La ruta va a recibir como parametro el nombre del post seleccionado

Se crea una variable con el directorio donde se encuentran los archivos html, dicho directorio funciona dinamicamente ya que se reemplaza el nombre del archivo html por el que va a ser recibido mediante la ruta de laravel

Si el archivo existe se van cargar la información mediante el método file_get_contents() de no ser así va a retornar un mensaje de error mediante el método php abort() 


```php
    Route::get('posts/{post}', function ($slug) {
        $path = __DIR__ . "/../resources/posts/{$slug}.html";

        if (!file_exists($path)) {
            abort(404);
        }

        $post = file_get_contents($path);

        return view('post', ['post' => $post]);
    });
```

# Modificación de la vista post
Modificamos la vista post ubicada en el directorio `/resources/wiews`

Eliminamos las etiquetas html que se encuentran dentro de la etiqueta article y sustituimos por una variable que va a ser el post dinamico obtenido por medio de la ruta


```html
    <body>
        <article>
            <?= $post;?>

        <a href="/">Go Back</a>
        
    </body>  
```





