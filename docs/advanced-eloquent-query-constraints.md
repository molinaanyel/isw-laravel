# Restricción de consultas avanzadas

A continuación vamos a crear un filtro para mostrar los Post por Categoría


## Creación del filtro
 
En el modelo Post, modificamos el método de filtrado que se creo anteriormente

Utilizamos el método `whereHas('category')` que hace referencia  a la relación que existe entre las dos tablas y con el método `where('slug,$category)` obtenemos el post por el slug de la categoría

```php
 public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, fn ($query, $search) =>
        $query
            ->where('title', 'like', '%' . $search . '%')
            ->orWhere('body', 'like', '%' . $search . '%'));

        $query->when(
            $filters['category'] ?? false,
            fn ($query, $category) => $query->whereHas(
                'category',
                fn ($query) => $query->where('slug', $category)
            )
        );
    }
```

Elminamos la ruta con el nombre categories y enviamos a la vista por medio del método index, la lista de categorias filtrada por slug

```php
 public function index()
    {
        return view('posts', [
            'posts' => Post::latest()->filter(request(['search', 'category']))->get(),
            'categories' => Category::all(),
            'currentCategory'   => Category::firstWhere('slug', request('category')),
        ]);
    }

```

