# Make the Newsletter Form Work

El primer paso es hacer el formulario dinámico para poder almacenar los datos de acuerdo a cada situación, esto se hace dentro del archivo `` web.php ``

```php
Route::post('newsletter'), function () {
    request()->validate(['email' => 'required|email']);

    $mailchimp = new \MailchimpMarketing\ApiClient();

    $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us6'
    ]);

    $response = $mailchimp ->lists->addListMember('nombreDeLaLista'), [
        'email_address' => request('email'),
        'status' => 'subscribe'
    ]
}

return redirect('/')->with('success', 'You are now signed up for our Newsletter');
 ```

Eso se encargará de guardar mediante el API de MailChimp.