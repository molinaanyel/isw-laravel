# Some light chapter clean up

En esta sección se harán validaciones, para depurar el código y mostrar los mensajes de error.

```php
    @error('body')
        <span>{{ $message }}</span>
    @enderror
 ```

 Esto hará que cuando no se ingrese texto en el comentario y se presione el botón para publicar genera un mensaje de error indicando que el campo es requerido.