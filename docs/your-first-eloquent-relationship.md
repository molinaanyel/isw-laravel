# Creación de la primera relación eloquent

A continuación los pasos para crear una relación eloquent

Para crear una relación necesitamos una nueva tabla con su respectivo modelo.

En este caso la tabla es categorias y la vamos asociar con la tabla posts

## Creación de la migración y modelo

```bash
    php artisan make:model Category -m   
```


```php
    public function up()
    {
            Schema::create('categories', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('slug');
                $table->timestamps();
            });
    }
```

## Creando la relación entre el modelo Post y Category


Modificamos la tabla post, agregando una llave foranea donde se va almacenar el id de la categoria a la que pertenece el post, mediante el método `foreignId()` que recibe como parámetro el nombre de la columna que se va a crear

```php
   public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('excerpt');
            $table->text('body');
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
        });
    }
```
Para establecer la relación identificamos cuál es la apropiada en este caso `belongsTo` ya que un post está asociado a una categoria

El método `belongsTo` recibe como parametro el modelo con el que queremos establecer una relación 

```php
  public function category()
  {
    return $this->belongsTo(Category::class)
  }

```

Tambien podemos acceder a las propiedades del modelo categorias por medio del modelo post, de la siguiente manera:

```bash
    $post->category->name

```

Con este obtenemos el nombre de la categoría a la que pertenece el post

