# Extract Form-Specific Blade component

En la ruta  `` post/create.blade.php `` se debe crear un file llamado  `` input.blade.php `` y dentro del directorio del formulario se puede hacer el título dinámico y que este se actualice de acuerdo a la publicación para ello se actualiza el título con la variable.  `` $name ``

Se hace lo mismo para todos los demás campos del formulario.

Se debe crear un file llamado  `` error.blade.php `` para hacer la validación de errores:

```php
@props(['name'])
    <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
 
 <x-form.error name="{{ $name }}" />
 
 ```

 Se hace este proceso para los label y como se mencionó cada uno de los campos del formulario.

 De esta forma cuando se haga una consulta al formulario este se actualizará de manera dinámica cargando la información que fue ingresada en la base de datos permitiendo reutilizar código.