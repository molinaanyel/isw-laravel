# Paginación

Podemos paginar mediante el método `paginate()` que recibe como párametro el número de elementos que se quieren mostrar


## Usando Paginate

```php

Implementamos el método paginate en el método index del controlador Post

  public function index()
    {
        return view('posts.index', [
            'posts' => Post::latest()->filter(
                request(['search', 'category', 'author'])
            )->paginate(18)->withQueryString()
        ]);
    }
```

De esta manera se generan los links 

`$posts->links()`

Modificamos el category-dropdown agregando el número de la página y mediante el método except() podemos ignorarlo y de está manera no va aparecer en el la url de la página por lo cual no nos afectara el método de  filtrar

```php
@foreach ($categories as $category)
        <x-dropdown-item
            href="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category', 'page')) }}"
            :active='request()->is("categories/{$category->slug}")'>
            {{ ucwords($category->name) }}
        </x-dropdown-item>
@endforeach

```


## Generando archivos de paginación

Digitamos en la consola el siguiente comando:

```bash
php artisan vendor:publish

```