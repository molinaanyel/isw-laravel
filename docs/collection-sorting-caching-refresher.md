# Almacenando y ordenando una colección 

A continuación los pasos para guardar en caché la lista de post y ordenarla


## Modificación del método all

Se utiliza el método `cache()->rememberForever` para almacenar la colección en caché, el cual recibe como parámetro el nombre bajo el cual vamos a guardar la información

También se implementó el método `sortByDesc();` para ordernar la lista de manera descendente

```php
    public static function all()
    {
        return cache()->rememberForever('post.all', function () {
            return collect(File::files(resource_path("posts")))
                ->map(fn ($file) => YamlFrontMatter::parseFile($file))
                ->map(fn ($document) => new Post(
                    $document->title,
                    $document->excerpt,
                    $document->date,
                    $document->body(),
                    $document->slug
                ))->sortByDesc('date');
        });
    }
```
