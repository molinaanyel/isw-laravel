# Implementando un menu dropdown

Se va a implementar un menu  dropdown mediante Javascript y el framework Alpine


Como primer paso se importa el cdn del framework alpine

`<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>`

 

 ## Creando un componente con Alpine
 
 Para crear un componente, creamos un div y declaramos el atibuto x-data esto recibe un objeto y mediante esta propiedad vamos a controlar cuando mostrar el menú, declaramos el estado como false


```html
 <div x-data="{ show: false }">

</div>

```
Creamos un botón con un evento click que se encarga cambiar el estado de visible u oculto y un div que va a contener las categorias
En el evento click validamos si la varibale show está en false va a cambiar a true y viceversa

```html
<div x-data="{ show: false }">
    
    <button @click="show = ! show">

    </button>

    <div x-show="show">

    </div>

</div>

```

Cargamos las categorías mediante un foreach que va a recorrer la lista de categorias que enviamos por medio de la ruta


```html
@foreach ($categories as $category)
    <a href="/categories/{{ $category->slug }}"
        class="block text-left px-3 text-sm leading-6
        hover:bg-blue-500 focus:bg-blue-500
        hover:text-white focus:text-white
       {{ isset($currentCategory) && $currentCategory->is($category) ? 'bg-blue-400 text-white' : '' }}">
        {{ ucwords($category->name) }}
    </a>
@endforeach

```



## Código completo


```html
        <div class="relative lg:inline-flex bg-gray-100 rounded-xl">
            <div x-data="{ show: false }" @click.away="show=false">

                <button @click="show = ! show"
                    class="flex-1 appearance-none bg-transparent py-2 pl-3 pr-9
                    text-sm font-semibold w-full lg:w-32 text-left inline-flex">

                    {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}

                    <svg class="transform -rotate-90 absolute pointer-events-none z-50" style="right: 12px; "
                        width="22" height="22" viewBox="0 0 22 22">
                        <g fill="none" fill-rule="evenodd">
                            <path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M21 1v20.16H.84V1z">
                            </path>
                            <path fill="#222"
                                d="M13.854 7.224l-3.847 3.856 3.847 3.856-1.184 1.184-5.04-5.04 5.04-5.04z">
                            </path>
                        </g>
                    </svg>
                </button>

                <div x-show="show" class="py-2 absolute bg-gray-100 w-full mt-2 rounded-xl z-50"
                    style="display: none;">
                    <a href="/"
                        class="block text-left px-3 text-sm leading-6
                         hover:bg-blue-500 focus:bg-blue-500
                          hover:text-white focus:text-white
                          ">
                        All
                    </a>
                    @foreach ($categories as $category)
                        <a href="/categories/{{ $category->slug }}"
                            class="block text-left px-3 text-sm leading-6
                            hover:bg-blue-500 focus:bg-blue-500
                            hover:text-white focus:text-white
                            {{ isset($currentCategory) && $currentCategory->is($category) ? 'bg-blue-400 text-white' : '' }}

                          ">
                            {{ ucwords($category->name) }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>

```





## Modificando las rutas

Modificamos el arhivo web.php que contiene las rutas

Enviamos una lista de categorías en las siguientes rutas:


```php
Route::get('/', function () {
    return view('posts', ['posts' => Post::latest()->get(),
    'categories'=> Category::all()]);
});


Route::get('categories/{category:slug}',function (Category $category) {
    return view('posts', ['posts' => $category->posts,
    'categories'=> Category::all()]);
});

Route::get('authors/{author:username}',function (User $author) {
    return view('posts', ['posts' => $author->posts,
'categories'=> Category::all()]);
});

```



