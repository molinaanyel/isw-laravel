# Conceptos básicos de blade

Blade es un motor de plantillas de Laravel que nos permite ingresar código php a una vista

Para hacer uso blade creamos un archivo con la extensión `blade.php` 
    `ejemplo.blade.php`

## @

El arroba es utilizado en blade para expresar bloques de código como lo son ciclos, condicionales ect

## {}

Con las llaves podemos imprimir el valor de una variable

## Cambios en las vistas
```html
    <!DOCTYPE html>
    <title>My Blog </title>
    <link rel="stylesheet" href="/app.css">

    <body>
        @foreach ($posts as $post)
            <article>
                <h1>
                    <a href="/posts/{{ $post->slug }}">
                        {{ $post->title }}
                    </a>
                </h1>
                <div>
                    {{ $post->excerpt }}
                </div>
            </article>
        @endforeach

    </body>


```


<!DOCTYPE html>

```html
    <title>My Blog </title>
    <link rel="stylesheet" href="/app.css">

    <body>
        <article>
            <h1>
                {{ $post->title }}
            </h1>
            <div>
                {!! $post->body !!}
            </div>
        </article>

        <a href="/">Go Back</a>

    </body>
```

