# Design the comment form

El siguiente paso es crear un formulario para los comentarios, el cual se puede crear en la ruta `` show.blade.php `` asociado a la variable `` $comment `` 

Este formulario debe contener una entrada de texto, un header, así como un botón para realizar la publicación.
