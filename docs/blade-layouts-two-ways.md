#  2 formas de utilizar layouts con blade

## Yield

Con Yield podemos crear una sección de contenido que podemos usar en otra vistas de la siguiente manera:
Usamos el método  `@extends('layout')` y dentro de este usamos   `@section('content')` que es donde va a estar el contenido

```php
    @extends('layout')
    @section('content')
        <article>
            <h1>
                {{ $post->title }}
            </h1>
            <div>
                {!! $post->body !!}
            </div>
        </article>

        <a href="/">Go Back</a>
    @endsection
 ```

## Implementando yield en una vista

Agregamos la etiqueta yield al body 
 ```php
    <!DOCTYPE html>

    <title>My Blog </title>
    <link rel="stylesheet" href="/app.css">

    <body>
        @yield('content')
    </body>
 ```




## Componentes

Los componentes funcionan de una manera similar 

Creamos un archivo .blade y abrimos una etiqueta con la siguiente sintaxis <x-nombredelcomponente>
dentro de dicha etiqueta vamos agregar el contenido 

 ```php
<x-layout>
    @foreach ($posts as $post)
        <article>
            <h1>
                <a href="/posts/{{ $post->slug }}">
                    {{ $post->title }}
                </a>
            </h1>
            <div>
                {{ $post->excerpt }}
            </div>
        </article>
    @endforeach
</x-layout>
 ```


## Implementando componentes en una vista

Agregamos la siguiente linea de código `{{$slot}}` y con esto esto indica donde se va a colocar contenido externo del componente

 ```php
    <!DOCTYPE html>

    <title>My Blog </title>
    <link rel="stylesheet" href="/app.css">

    <body>
        <h1>hola</h1>
        {{$slot}}
    </body>


 ```


