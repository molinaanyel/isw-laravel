# Crear el formulario de Post

Se debe crear un ` Form Input ` el cual debe contener un 

* Panel Component
* Título
* Cuerpo
* Submit Button
* Selector de categoría

Es importante considerar que cada elemento contiene un apartado ` name ` el cual será capturado al presionar el botón de enviar.

La sección de categoría se conecta con la lista de categorías para que así lo pueda cargar, con el código

```php
@foreach ($categories as $category)
    <option value="{{category ->id }}">{{ ucwords($category->name )}}</option>
@endforeach
 ```

Luego en el archivo ` web.php ` se debe agregar la línea

```php
Route:post('admint/posts, [PosController::class, 'store'])->middleware('admin');
 ```

 El cual se encargará de guardar la información capturada del formulario, adicional se debe crear la función ` store() ` conectada al ` Submit Button `

 ```php
public function store()
{
    ddd(request()->all());
}
 ```