# Configuración del ambiente para usar una base datos

A continuación las instrucciones para la configuración y creación de la base datos


## Creación de la base de datos

Desde la máquina virtual corremos los siguientes comandos

```bash
    sudo mysql
    create user user_laravel identified by 'secret';
    CREATE DATABASE IF NOT EXISTS db_laravel;
    grant all privileges on db_laravel.* to user_laravel;
    flush privileges;
    quit;
```


## Configuración archivo .env

En el arhivo .env agregamos nuestros datos para crear la conexión a la base de datos

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_laravel
DB_USERNAME=user_laravel
DB_PASSWORD=secret

## Finalización de la configuración de la base de datos

Nos posicionamos en la maquina virtual en el directorio `cd /vagrant/sites/lfts.isw811.xyz/` y corremos el siguiente comando:

```bash
    php artisan migrate
```
Obtendremos una salida como esta si la configuración se realizó con exito 

Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (18.40ms)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (10.98ms)
Migrating: 2019_08_19_000000_create_failed_jobs_table
Migrated:  2019_08_19_000000_create_failed_jobs_table (9.24ms)
Migrating: 2019_12_14_000001_create_personal_access_tokens_table
Migrated:  2019_12_14_000001_create_personal_access_tokens_table (15.40ms)


