# Encontrar un paquete Composer para publicar metadatos

A continuación  los pasos para instalar un paquete de composer para leer archivos en formato yalm

## Instalación del paquete

Digitamos el siguiente comando en nuestra consola para la instalación del paquete `yaml-front-matter`

```shell
    composer require spatie/yaml-front-matter
```

## Modificación del modelo

Agregamos el constructor con sus propiedades

```php
class Post
{

    public $title;
    public $excerpt;
    public $date;
    public $body;
    public $slug;

    public function __construct($title, $excerpt, $date, $body, $slug)

    {
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
        $this->slug = $slug;
    }

}
```

## Modificación de los archivos html

Se agregan las cabeceras a cada archivo

``html
---
title: My First post
slug: my-first-post
excerpt: Lorem ipsum dolor sit, amet consectetur adipisicing elit.
date: 2021-05-21
---

<p>
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique
    quibusdam deserunt provident nesciunt esse cupiditate alias, cumque sapiente
    pariatur odit nisi architecto maxime fugiat doloribus saepe, tenetur quia
    tempore asperiores?
</p>
```



## Creación de los métodos

Creamos la función all que obtiene todos nuestros achivos yaml y los convierte a objetos php mediante la función `parseFile()` y luego con la función  `map` los convertimos a objetos de tipo Post

```php
    public static function all()
    {
        return collect(File::files(resource_path("posts")))
            ->map(fn ($file) => YamlFrontMatter::parseFile($file))
            ->map(fn ($document) => new Post(
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body(),
                $document->slug
            ));
    }


```


## Creación del método de búsqueda

Creamos un método para filtrar post por slug, mediante la función `firstWhere()` de php que nos retornara el post que el slug coincida con el obtenido en el parámetro

```php
    public static function find($slug)
        {
            return static::all()->firstWhere('slug', $slug);
        }
```


## Agregando cambios a la las vistas

se reemplaza el contenido anterior por el siguiente:

Se mostró el titulo y el excerpt accediendo a las propiedades del objeto Post

```html
    <!DOCTYPE html>

    <title>My Blog </title>
    <link rel="stylesheet" href="/app.css">

    <body>
        <?php foreach ($posts as $post) : ?>
        <article>
            <h1>
                <a href="/posts/<?= $post->slug ?>">
                    <?= $post->title ?>
                </a>
            </h1>
            <div>
                <?= $post->excerpt ?>
            </div>
        </article>
        <?php endforeach; ?>

    </body>
```

```html
    <!DOCTYPE html>

    <title>My Blog </title>
    <link rel="stylesheet" href="/app.css">

    <body>
        <article>
            <h1>
                <?= $post->title ?>
            </h1>
            <div>
                <?= $post->body ?>
            </div>
        </article>

        <a href="/">Go Back</a>

    </body>
```












