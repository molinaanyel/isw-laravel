# Build the login page

Dentro de `` web.php `` se debe crear la ruta para referenciar el archivo de `` SessionController.php `` 

Para la página de Log-in se puede reutilizar un formulario creado anteriormente que contenga:

* Input Nombre de usuario
* Input Contraseña
* Botón de Log In
* Validar que los campos no estén vacíos cuando se presiona el botón Log In

Luego este formulario se debe conectar a un función que permita validar si el usuario existe en la base de datos, esta validación

```php
function store() {
    'email' => 'required|email',
    'password' => 'required'

if (auth()->attempt($attributes)) {
    return redirect('/')->with('sucess', 'WelcomeBack');
}
return back()->withErrors();
}
 ```

 Esto hará que si efectivamente existe muestre la página de inicio de lo contrario dará un mensaje de error.