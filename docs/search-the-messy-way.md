# Búsqueda de manera(De manera no optimizada)

A continuación vamos a implementar una barra de búsqueda


## Consulta a la base datos

Obtenemos la entrada que el usuario digita en el input y por medio de una consulta a la base de hacemos la búsqueda

A travez del método `where()` y `orwhere` buscamos por titulo y body

```php
if(request('search')){
    $posts
    ->where('title','like','%',. request('search') . '%');
    ->orWhere('body','like','%',. request('search') . '%');
}
```

## Código completo


```php
Route::get('/', function () {
    $posts = Post::latest();

    if (request('search')) {
        $posts->where('title', 'like', '%' . request('search') . '%')
            ->where('body', 'like', '%' . request('search') . '%');
    }

    return view('posts', [
        'posts' => $posts->get(),
        'categories' => Category::all()
    ]);
})->name('home')

```