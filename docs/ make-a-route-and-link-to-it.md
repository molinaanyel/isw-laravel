# Creación de una ruta y vincularla a una vista


## Modificamos la vista welcome.blade.php
Sustituimos el nombre de la vista "welcome.blade.php" por "posts.blade.php"

Modificamos su contenido con el siguiente contenido:
```html
    <!doctype html>

    <title>My Blog</title>

    <link rel="stylesheet" href="/app.css"> 

    <body>
        <article>
            <h1><a href="/post"> My Firts Post</a></h1>  

            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates aut rerum perferendis quod a molestiae pariatur, beatae reprehenderit consequatur, ex recusandae minus quis exercitationem at saepe officiis et. Modi!      
            </p>

        </article>

        <article>
            <h1><a href="/post"> My Second Post</a></h1>  
            
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates aut rerum perferendis quod a molestiae pariatur, beatae reprehenderit consequatur, ex recusandae minus quis exercitationem at saepe officiis et. Modi!
            </p>

        </article>

        <article>
            <h1><a href="/post"> My Third Post</a></h1>  
            
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates aut rerum perferendis quod a molestiae pariatur, beatae reprehenderit consequatur, ex recusandae minus quis exercitationem at saepe officiis et. Modi!
            </p>

        </article>
        
    </body>
```


## Modificación del archivo de rutas
Ingresamos a la carpeta routes localizada en `/routes/web.php` y modificamos el archivo web.php
reemplazamos el nombre de la vista por el nombre actualizado "posts"
 ```php
        Route::get('/', function () {
            return view('posts');
        });

```


## Creación de la ruta
Nos posicionamos en la carpeta routes `/routes/web.php` en el archivo web.php y agregamos la siguiente 

ruta
 ```php
        Route::get('post', function () {
            return view('post');
        });

```


## Creación de la vista para mostrar un post
Nos posicionamos en la carpeta resources `/resources/wiews` y creamos una vista con el nombre "post" 
"post.blade.php" y agregamos el siguiente contenido:
 ```html
    <body>
    <article>
        <h1><a href="/post"> My Firts Post</a></h1>  

        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates aut rerum perferendis quod a molestiae pariatur, beatae reprehenderit consequatur, ex recusandae minus quis exercitationem at saepe officiis et. Modi!      
        </p>

    </article>

    <a href="/">Go Back</a>
     
</body>

```

## Modificamos el archivo de estilos (app.css)
Modificamos el archivo de estilos que se encuentra en  `/public/app.css` 
```css
    body { 
        background: white;
        color: #222222;
        max-width: 600px;
        margin: auto;
        font-family: sans-serif;
    }

    p{
        line-height: 1.6;
    }

    article + article{
        margin-top: 3rem;
        padding-top: 3rem;
        border-top: 1px solid #c5c5c5;
    }
    
```






       
