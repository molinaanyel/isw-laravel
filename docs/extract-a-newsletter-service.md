# Extract a Nesletter Service

En el `` app directory `` se debe crear un directorio llamado `` Services/Nesletter.php `` 

```php
public function subscribe($email) {
    $mailchimp = new ApiClient():

    $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us6'
    ]);

    return $mailchimp->lists->addListMember('nombreDeLaLista'), [
        'email_address' => $email,
        'status' => 'subscribed'
    ]
};
 ```

 Luego desde `` Newsletter.php `` se puede obtener la lista de subscriptores con el código

 ```php
return $mailchimp->lists->addListMember(config('services.mailchimp.lists.subscribers'), [
    'email_address' => $email,
    'status' => 'subscribed'
]);
 ```

 Con esto se valida que si el usuario está subscrito no pueda subscribirse nuevamente.
