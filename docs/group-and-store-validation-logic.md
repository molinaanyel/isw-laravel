# Group and store validation logic

Lo primero que se debe hacer evitar duplucidad para ello se puede hacer una validación si el Post existe entonces por ejemplo la imagen en miniatura es requerida si el post no existe, pero si existe la validación no será requerida.

Un ejemplo de la validación evitando duplicidad sería:

```php
    $post->exists ? ['image'] :['required', 'image'],
 ```

 