# Mostrando los Post por author

A continuación vamos a filtrar todos los posts relacionados a un usuario


## Modificación del Modelo Post

Con el fin de reflejar el código a un lenguaje más natural cambiamos el nombre de la relación user, sustituimos el nombre por author

Al cambiar el nombre de la relación tenemos que especificar cuál va a ser el id, ya que automaticamente laravel toma el id como el nombre del la relación author_id y el campo author no existe en la tabla users

```php
  public function author()
    {
        return $this->belongsTo(User::class,'user_id');
    }
```



## Modificación de la tabla Users

Creamos un  nuevo campo en la tabla users para filtrar los Post por un nombre de usuario `$table->string('username')->unique();`

```php
    public function up()
        {
            Schema::create('users', function (Blueprint $table) {
                $table->id();
                $table->string('username')->unique();
                $table->string('name');
                $table->string('email')->unique();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->rememberToken();
                $table->timestamps();
            });
        }
```


## Modificación del UserFactory

 Agregamos el campo username que creamos anteriormente `'username' => $this->faker->unique()->userName()`

```php
    public function definition()
        {
            return [
                'name' => $this->faker->name(),
                'username' => $this->faker->unique()->userName(),
                'email' => $this->faker->unique()->safeEmail(),
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ];
        }
```
## Creación de una nueva ruta

Creamos una nueva ruta en el archivo de rutas web.php

Esta ruta va a filtrar todos los posts de un usuario 

Va a recibir como parámetro el nombre de usuario del autor del post

```php
    Route::get('authors/{author:username}',function (User $author) {
        return view('posts', ['posts' => $author->posts]);
    });
```

También modificamos la ruta que muestra todos los posts

Por medio del Modelo Post con método `with()` obtenemos todos los post con su autor y categoría


```php
Route::get('/', function () {
    return view('posts', ['posts' => Post::with(['category','author'])->get()]);
});

```


## Modificación del las vistas

Sustituimos la etiqueta `<p>` por lo siguiente


```html
<p>
    By <a href="#">{{$post->author->name}}</a> <a href="/categories/{{$post->category->slug}}">{{ $post->category->name}}</a>
</p>

```