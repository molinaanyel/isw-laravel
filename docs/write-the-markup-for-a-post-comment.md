# Write the markup for a post comment

En el apartado `` show.blade.php `` se debe crear un `` header `` que contenga el estado del post, así como la fecha de publicación, el autor, el cual se puede ajustar con CSS para alinearlo acorde a lo esperado.

Al utilizarlo de forma dinámica se puede reciclar el elemento para poder generar así este mismo apartado para todos los posts.
