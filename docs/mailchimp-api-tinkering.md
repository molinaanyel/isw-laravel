# Mailchimp API Tinkering

En este sección se conectará el botón de subscripción con el API del sistema de correos llamado Mailchimp.

Primero se debe ir a `` components/layout.blade.php `` y se deben asignar los ids de Newsletter para redirigir cuando se de click en la opción, además con los estilos CSS se puede hacer que el movimiento sea lento con el siguiente código

```CSS
scroll-behavior: smooth;
 ```

 Luego desde la página de Mailchimp se procede a registrar una cuenta gratuita.

 Desde el apartado Account - Extra - APIs Keys. Esta llave se debe copiar en el archivo `` .env `` y se puede declarar como

 ```php
MAILCHIMP_key=
 ```

 Luego en `` config/services.php `` se escribe el código

 ```php
'mailchimp' => [
    'key' => env('MAILCHIMP_KEY')
]
 ```

Luego dentro de composer se debe importar la librería

```php
composer require mailchimp/marketing
 ```

 Y esta instrucción se encarga de importar la librería. La cual se debe validar con el código de ejemplo contenido en la documentación oficial de Mailchimp. 

 Posteriomente se procede a actualizar el código de Mailchimp para que cumpla con los campos del formulario.