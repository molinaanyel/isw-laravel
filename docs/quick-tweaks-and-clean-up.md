# Algunos ajustes rápidos y limpieza

A continuación algunos cambios y ajustes en los componentes del proyecto



## Drowpdown

Le establecemos una altura máxima al componente dropdown


```html
<div x-show="show" class="py-2 absolute bg-gray-100 w-full mt-2 rounded-xl z-50 overflow-auto   max-h-52" style="display: none;">
        {{ $slot }}
</div>

```

## Header

Se elimina el párrafo que se encuentra en el header y el la etiqueta `<h2>`

Disminuimos el marging top del primer `<div>` 
    `<div class="space-y-2 lg:space-y-0 lg:space-x-4 mt-4">`


## Factories

Con el fin de cambiar el tamaño de las propiedades del post excerpt y body, vamos a modificar el tipo de dato que quieremos generar en el PostFactory

Cambiamos de una oración a 2 párrafos, con el fin de no perder el espaciado generamos estos datos como párrafos html por lo cual vamos a implementar HTML scaping

 `'excerpt' => $this->faker->paragraph(2);`

```php
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'category_id' => Category::factory(),
            'title' => $this->faker->sentence,
            'slug'=> $this->faker->slug,
            'excerpt' => '<p>' . implode('</p><p>', $this->faker->paragraphs(2)) . '</p>',
            'body' => '<p>' . implode('</p><p>', $this->faker->paragraphs(6)) . '</p>',

        ];
    }
```