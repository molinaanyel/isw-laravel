# Failed validation and old input date

En esta sección se asocian las validaciones con los datos contenidos en el proyecto, para ello se crean errores para:

* Cuando el nombre no cumple los requisitos
* La contraseña no contiene los carácteres solicitados
* El correo no cumple los requerimientos

De esta forma nos aseguramos que no se guardarán entradas erróneas en la base de datos sino que tendremos siempre información correcta.