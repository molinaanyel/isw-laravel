# Listando todos los posts pertenecientes a una categoría

A continuación los pasos para mostrar todos los post de una categoría.


## Creación de una nueva ruta 

Creamos una nueva ruta para mostrar todos los post asociados a una categoria y usamos Route Model Binding

```php
    public function posts()
    {
        
    Route::get('categories/{category:slug}',function (Category $category) {
    return view('posts', ['posts' => $category->posts]);
    } );


    }
```


## Establecemos la relación de  categorias con post 

Como una categoría puede tener muchos posts la relación sería `hasMany`

```php
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
```

## Modificación de las vistas

Modificamos la vista `post` y `posts`

En  el enlace que contiene el nombre de la categoría escribimos la ruta que creamos anteriormente y le pasamos con parámetro el slug de la categoría

Con esto vamos a mostrar todos los posts asociados a una categoría

```html
    <a href="/categories/{{$post->category->slug}}">{{ $post->category->name}}</a>

```