# Usando la clase del sistema de archivos para leer un directorio

Con el fin de limpiar nuestro código que realiczamos para mostrar cada post seleccionado vamos a utilizar los métodos del sistema de archivos y modelos



## Creación de un modelo

En la carpeta Models creamos una clase php con el nombre Post

```php
    namespace App\Models;

    class Post
    {
         
    }
```

## Creación del método para fitrar post

Creamos un método estatico en nuestra clase Post para filtrar los post 

Hacemos uso del helper `resource_path` para obtener la ruta de la carpeta resources donde están almacenados los archivos html de cada post

validamos si el post seleccionado existe en caso de no cumplirse la validación lanzamos una execepción con ` throw new ModelNotFoundException()` 


```php
 
    public static function find($slug)
    {
        if (!file_exists($path = resource_path("posts/{$slug}.html"))) {
            throw new ModelNotFoundException();
        }

        return cache()->remember("post.{$slug}", 1200, fn () => file_get_contents($path));
    }
```

## Creación del método para obtener todos los post

Implementamos la función `files()` que nos permite leer un directorio de esta forma podremos leer todos los archivos archivos html de la carpeta post

Haciendo uso del método de laravel `array_map` obtenemos un nuevo array con el contenido de todos los post

```php
 public static function all()
    {
        $files = File::files(resource_path('posts/'));

        return array_map(fn ($file) => $file->getContents(), $files);
    }
    
```


## Implementación del modelo en la ruta
Importamos el modelo Post en nuestro archivo de rutas web.php `use App\Models\Post` 

Modificamos la ruta  `/ ` borramos el código anterior  y y llamamos al método creado anteriormente para obtener todos los posts, de la siguiente manera `Post::all()`


Modificamos la ruta  `posts` borramos el código anterior y llamamos al método creado anteriormente para filtrar posts, de la siguiente manera `Post::find($slug)`


```php
    use Illuminate\Support\Facades\Route;

    use App\Models\Post;

    Route::get('/', function () {
        return view('posts', ['posts' => Post::all()]);
    });

    Route::get('posts/{post}', function ($slug) {
        return view('post', ['post' => Post::find($slug)]);
    })->where('post', '[A-z_\-]+');
```


## Cambios en la vista
Sustituimos lo que está dentro de la etiqueta `body` por el siguiente código:

Con esto vamos a obtener dinamicamente los post enviados a travez de la ruta

```php
    <?php foreach ($posts as $post) : ?>
        <article>
            <?= $post ?>
        </article>
    <?php endforeach; ?>
```



