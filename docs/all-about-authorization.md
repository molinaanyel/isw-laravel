# All about authorization

En esta sección se declara la lógica de autorización. Dentro de la ruta `` /AppServiceProvider.php ``

```php
Gate::define('admin', function (user $user){
    return $user->username == ;
}
 ```

 Esto se encargará de tomar la información del usuario actual que está logueado y validará si es administrador o no.

En el `` PostController.php `` se puede ingresar el siguiente método

```php
    $this->authorize('admin');
 ```

 El método `` $this `` muestra el error 403 si el usuario no pasa la validación.

 En el `` layout.blade.php `` se puede agregar la validación

 ```php
@if (auth()->user()->can('admin'))
@endif
 ```

 Que se encarga de mostrar los links que requieren acceso de administrador, de lo contrario entonces no los mostrará y devolerá el error 403
 