# Migraciones y sus fundamentos

A continuación los principios básicos de las migraciones

La migraciones nos permiten modificar la base de datos desde la terminal por medio de distintos comandos que relizan una acción  específica

##  Migrate

Crea las tablas en la base de datos

```bash
    php artisan migrate
```

## Migrate:rollback

Revierte la última migración

```bash
    php artisan migrate:rollback
```

##  Migrate:fresh

Borra la base datos y la crea nuevamente vacia

```bash
   php artisan migrate:fresh
```


