
# Implementado diseño(CSS Y JS)


## Creación del archivo CSS

Nos posicionamos en la carpeta public y creamos un archivo con la extensión css
Escribimos el siguiente código dentro del archivo 
```css
    body { 
        background: navy;
        color: white;
    }
```
Con esto estamos modificando el color del fondo de la página y las letras 


## Integrando CSS en la vista

Agregamos la siguiente linea a nuestra vista donde en el atributo href se especifica la ruta del archivo css
    ```html <link rel="stylesheet" href="/app.css">
    ```



## Creación del archivo JS
Nos posicionamos en la carpeta public y creamos un archivo con la extensión js
Escribimos el siguiente código dentro del archivo 
```javascript 
    alert("I am here");
```


## Integrando JS en la vista

Agregamos la siguiente linea a nuestra vista donde en el atributo src se especifica la ruta del archivo js
    ```html <script src="/app.js"></script>
    ```




