# Uniendo el Query de Categorías y Búsqueda

A continuación vamos a unir los querys de categorías y búsqueda con el fin de crear un nuevo filtro en el que podamos obtener una Post por categoría y por búsqueda simultaneamente

obtenemos los datos del formulario mediante el método Get, validamos si selecionamos una categoría y guardamos el valor en un input oculto

```html
 <form method="GET" action="/">
    @if (request('category'))
        <input type="hidden" name="category" value="{{ request('category') }}">
    @endif
    <input type="text" name="search" placeholder="Find something"
        class="bg-transparent placeholder-black font-semibold text-sm" value="{{ request('search') }}">
</form>

```

Modificamos el category-dropdown agregando el siguiente código

```html
 @foreach ($categories as $category)
        <x-dropdown-item
            href="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category', 'page')) }}"
            :active='request()->is("categories/{$category->slug}")'>
            {{ ucwords($category->name) }}
        </x-dropdown-item>
@endforeach
```