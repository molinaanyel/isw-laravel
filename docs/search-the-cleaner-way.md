# Implementando una barra de búsqueda de manera optimizada

A continuación vamos a crear una barra de búsqueda de manera apropiada utilizando controladores y métodos.

## Creación del Controlador

Con el siguiente comando creamos el controlador para los Posts, la sintaxis a seguir es el nombre del controlador ya sea en plural o singular más la palabra Controller

```bash
    php artisan make:Controller PostController

```

## Métodos

Se crea un método de busqueda en el elocuent model Post

Este método nos permite construir un query para hacer una consulta en la base de datos

recibe como párametro un array con el filtro de busqued digitado por el usuario

Mediante el método when validamos si el array tiene contenido

```php
 public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, fn ($query, $search) =>
        $query
            ->where('title', 'like', '%' . $search . '%')
            ->orWhere('body', 'like', '%' . $search . '%'));
    }
```



Creamos un método en el controladro el cual de va a retonar la lista de Posts filtrada meidiante el uso del método que creamos en el modelo Post

```php

public function index()
    {
        return view('posts', [
            'posts' => Post::latest()->filter(request(['search']))->get(),
            'categories' => Category::all()
        ]);
    }

```

Muestra un post 
```php
 public function show(Post $post)
    {
        return view('post', [
            'post' => $post
        ]);
    }
```

## Rutas y Controladores

Para implementar los métodos creados tenemos que llamar al controlador en la ruta

La ruta recibe como párametro el nombre de la ruta y entre corchetes el controlador y el método que queremos asignar a esa ruta

```php
Route::get('/',[PostController::class,'index'])->name('home')

Route::get('posts/{post:slug}', [PostController::class, 'show']);
```