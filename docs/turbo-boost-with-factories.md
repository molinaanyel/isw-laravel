# Factories

Los factories son una funcionalidad de Laravel que nos permite generar datos de prueba


## Creación de un Factory

Ejecutamos el siguiente comando desde nuestra máquina virtual


 
 ```bash
    php artisan make:factory NombreDelModeloFactory
 ```

Una vez creado, abrimos el archivo generado en la ruta database/factories/PostFactory.php

En la función `definition()` creamos un array con los atributos del post siguiendo la siguiente sintaxis

`['nombre_atributo'=> $this->fake->sentence]`

faker es una libreria que nos permite asignar el tipo de dato falso que queremos generar

También podemos establecer algunos parámetros con un valor por defecto

En este caso especificamos que el atributo nombre tendrá el valor John Doe por defecto y los demás valores van a ser generados 

`User::factory()->create(['name' => 'John Doe']);`

## Creando Relación de registros entre un Post, Usuario y Categoría

Se puede asignar al atributo del id_category y id_user el método factory de los modelos Category y User, esto va a crear una categoría y un usuario por cada post 
además de asignar el id automaticamente a los atributos id_category y user_id

 
 ```php
    'user_id' => User::factory(),
    'category_id' => Category::factory(), 
 ```