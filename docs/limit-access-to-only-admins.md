# Limit Access to Only Admins

## Dedicated Form

Primero se debe ir a la ruta ` File ` y se ingresa la siguiente instrucción

```php
Route::get('admin/posts/create', [PostController::class, 'create']);
 ```

Luego se debe crear un formulario dentro del nuevo archivo ` PostControler.php ` 

```php
public funtion create()
{
    return view('posts.create');
}

 ```

 Se debe agregar un nuevo archivo dentro del directorio ` resources/views/posts/ ` se puede reutilizar el contenido del archivo ` resources/views/posts/show.blade.php ` para lo cual se puede agregar el código

 ```php
<x-layout>
    <section class="py-6 py-8">
        Hello
    </section>
</x-layout>
 ```

Luego se debe ir a la dirección ` 127.0.0.1:8000/admin/posts/create ` para verificar que se haya creado correctamente.

## Como hacer la página visible para solo administradores

Dentro del archivo ` PostController.php ` se puede hacer una validación 

```php
public function create()
{
    if (auth()->guest()) {
        abort(403);
    }
};
 ```

También se puede actualizar la tabla de los usuarios y crear una Clase llamada ` MustBeAdministrator ` que contenga una función que valide si el usuario
