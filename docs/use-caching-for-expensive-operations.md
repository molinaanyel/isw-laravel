# Utilizando el almacenamiento caché

Guardar la información en caché es una alternativa para agilizar el proceso de carga de la información

Para esto podemos utilizar la función `cache()->remember()` que recibe como parametro un id y lapso de tiempo que se desea guarda la información

```php
    Route::get('posts/{post}', function ($slug) {


        if (!file_exists($path = __DIR__ . "/../resources/posts/{$slug}.html")) {
            abort(404);
        }

        $post = cache()->remember("post.{$slug}", 1200, fn () => file_get_contents($path));

        return view('post', ['post' => $post]);
    })->where('post', '[A-z_\-]+');

```
