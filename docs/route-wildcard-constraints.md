# Restricciones de comodín de rutas 

Laravel cuenta con una serie de expresiones regulares que podemos aplicar a los parámetros que vamos a recibir en una ruta para controlar su formato


## Método Where
Este método recibe como parámetro el nombre del parámetro que va a recibir la ruta y las expresiones regulares de como queremos que esté construido su formato
```php
    Route::get('posts/{post}', function ($slug) {
        $path = __DIR__ . "/../resources/posts/{$slug}.html";

        if (!file_exists($path)) {
            abort(404);
        }

        $post = file_get_contents($path);

        return view('post', ['post' => $post]);
    })->where('post', '[A-z_\-]+');
```

## Otros Métodos
Laravel también cuenta con otros métodos como lo son:
 `whereAlphaNumeric` Acepta números y letras
 `whereNumber` Solo acepta números