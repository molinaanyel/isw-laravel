# Validando y guardando miniaturas del post


Primero se debe incluir un `` Input `` para la miniatura 

En la ruta `` config/filesystem.php `` viene la configuración predeterminada del sistema para gestionar archivos subidos 

Luego se debe agregar el `` Path `` a la miniatura guardada, además se recomienda guardarlo en el `` Public Disk `` para que pueda ser accesible a la vista pública de los usuarios

Se debe correr la línea de código

```php
artisan migrate:fresh --seed
 ```
Para actualizar las tablas y que permitan que las imágenes queden asociadas a la publicación

Dentro del  `` PostController.php `` se debe agregar la instrucción que dentro del formulario sea requerido agregar una miniatura para la publicación mediante el código

```php
$atributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
 ```

 La cual lo convertirá en un campo obligatorio y se encargará de a la vez de guardarlo en el campo correspondiente en la base de datos.
 