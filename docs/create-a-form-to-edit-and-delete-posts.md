# Create a form to edit and delete posts

Se debe crear un nuevo post controller con la instrucción 

```php
artisan make:controller Admin
 ```

 Luego dentro del nuevo archivo creado se agrega el código:

 ```php
public function index()
    {
        return view('admin.posts.index') [
            'posts' => Post::paginate(50)
        ]
    };
 ```

 Se puede importar una tabla ya creada y ajustarla a los requerimientos y luego se cambian los textos por los valores dinámicos asociados a las publicaciones.

 El URL de Edit se debe asociar al post que se desea editar.
