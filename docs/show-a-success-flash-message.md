# Show a success flash message

Dentro del `` RegisterController.php `` se agrega

```php
User::created($attributes);

session()->flash('success', 'Your account has been created');

return redirect('/');
 ```

 Esto mostrará a los usuarios que completaron el registro que la cuenta fue creada de forma satisfactoria.

 Dentro de `` layout.blade.php `` se debe crear

 ```php
@if (sessicion()->has('success'))

@endif
 ```

 Se anida un elemento `` div `` que contenga la validación de que la sesión fue satisfactoria.