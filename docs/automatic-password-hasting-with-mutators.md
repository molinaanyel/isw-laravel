# Hasting Automatico de contraseñas

Mediante el método `bcryp()``encriptamos la contraseña

Creamos un método en el modelo User que va a encriptar la contraseña automáticamente


```php
 public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    } 
```  

## Maneras de validar contraseñas

Si queremos validar la contraseña con la contraseña encriptada podemos realizarlo mediante tinker


```bash
    $jane =  App\Models\Usser::(find);
    $jane->password;
    Iluminate\Support\Facades\Hash::check('password', $jane->password)
```