# Make the comments table dynamic

En esta sección se hará la sección de comentarios dinámica para asegurarnos de poner reutilizar el código y que así podamos acceder a los archivos de la base de datos y mostrar los comentarios de esta forma dinámica.

Dentro de `` CommentFactory.php `` se debe crear la función

```php
function definition() {
    return [
        'post_id' => Post::factory(),
        'user_id' => Post::factory(),
        'body' => $this->faker->paragrah
    ]    
}
 ```

 Luego se ingresa el código

 ```php
php artisan tinker
App\Models\Comment:factory()->create();
 ```

 Para asegurarnos que dentro del directorio factory se puedan crear estas relaciones como asociar el post con el comentario, poder acceder los comentarios del post, etc.