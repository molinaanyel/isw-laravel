# Extraer un componente de categoría de una lista desplegable


## Creación de un componente

Creamos un componente a travez de la terminal y asociamos una clase al mismo


```bash
    php artisan make:component CategoryDropdown
```

agregamos el contenido del dropdown que está en el header


```html
<x-dropdown>
                <x-slot name="trigger">
                    <button class="py-2 pl-3 pr-9 text-sm font-semibold w-full lg:w-32 text-left flex lg:inline-flex">
                        {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}

                        <x-icon name="down-arrow" class="absolute pointer-events-none" style="right: 12px;" />
                    </button>
                </x-slot>

                <x-dropdown-item href="/" :active="request()->routeIs('home')">All</x-dropdown-item>

                @foreach ($categories as $category)
                    <x-dropdown-item href="/?category={{ $category->slug }}" :active='request()->is("/{$category->slug}")'>
                        {{ ucwords($category->name) }}</x-dropdown-item>
                @endforeach
    </x-dropdown>

```

## Clase asociada

En la clase CategoryDropdown pasamos la variable categories al método render


```php
 public function render()
    {
        return view('components.category-dropdown',[
            'categories'=> Category::all();
        ]);
    }

```

Al tener un componente asociado a una clase para las categorías no se necesita obtener la lista de categorias en las rutas y controlador del Post



```php
   public function index()
    {
        return view('posts', [
            'posts' => Post::latest()->filter(request(['search', 'category']))->get(),
        ]);
    }
```



```php
Route::get('/', [PostController::class, 'index'])->name('home');

Route::get('posts/{post:slug}', [PostController::class, 'show']);

Route::get('authors/{author:username}', function (User $author) {
    return view('posts', [
        'posts' => $author->posts,
    ]);
});

```





