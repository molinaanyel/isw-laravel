# Asignamiento Másivo

Por defecto Laravel no permite a los modelos guardar arrays, tenemos que  agregar los valores uno por uno.


## 3 Formas de moderar las vulnerabilidades del Asignamiento Másivo

A continuación 3 formas en las que podemos lidiar con esto:

Mediante la palabra reservada `fillable` declaramos una variable y como valor asignamos un array con los valores de los campos que podemos ingresar con asignación másiva


```php

    protected $fillable= ['title','body']  

```



Otra manera es usando `guarded` asigna en un array los valores que no se pueden ingresar con asignación másiva



```php
    protected $guarded =  ['id']
```

Por último la tercer manera es no usando asignación másiva y se podría representar de la siguiente manera:


```php
    protected $guarded =  []
```





