# Cargar relaciones en un modelo existente

En los capitulos anteriores usamos el método with() en todas rutas en las que se necesitó cargar las categorías y autores de los post


## With

Hay una forma más simple y optimizada de hacerlo y es mediante el método with 

En el modelo declaramos un arreglo con el nombre $with,definimos las relaciones que queremos que se carguen por defecto

```php
    protected $with= ['category','author']
```

De esta manera no se necesita usar el método with en las rutas

En caso de no necesitar la carga de alguna relación, se puede desactivar mediante el método `without()` que recibe como parámetro el nombre de la relación


```bash
    App\Models\Post::without('author')
```
