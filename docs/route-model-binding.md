# Route Modelo Binding

Es un mecanismo de Laravel que nos permite usar una instancia de un modelo en una ruta

## Usando Route Modelo Binding

Modificamos el archivo de rutas

En lugar del id del post la función recibe como párametro una instancia de un Post

El nombre de la instancia debe coincidir con el parametro que va a recibir la ruta 

Automáticamente laravel va a buscar los registros según su id

```php
    Route::get('posts/{post}', function (Post $post) {
        return view('post' =>$post);
    });
```


También podríamos especificar el valor por el cuál quisieramos filtrar los registros 

En este ejemplo se van a filtrar los registros de posts por el slug

Modificamos la tabla posts, agregamos un nuevo campo `slug`

```php
 public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('excerpt');
            $table->text('body');
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
        });
    }

```

```php
    Route::get('posts/{post:slug}', function (Post $post) {
        return view('post' =>$post);
    });
```




