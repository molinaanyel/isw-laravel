# Toy chests and contracts

En la ruta `` NewsletterController.php `` dentro de la clase NewsletterController se debe crear un `` container `` para los Newsletters.

Luego con la función siguiente se encargará de registrar al usuario a un Nesletter Provider en específico.

```php
function register() {
    app()->bind(Nesletter::class, function (){
        $client = new ApiClient();

        $client=>setConfig([
            'apiKey' => config('services.mailchimp.key')
            'server' => 'us6'
        ]);

        return new Nesletter($client);
    })
}
 ```

Con esto se podrá almacenar de una forma más ordenada y limpia sin redundancias de código si manejamos más de un proveedor de Newsletter.