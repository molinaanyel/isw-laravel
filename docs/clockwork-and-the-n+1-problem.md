# Clockwork

Es una herramienta que nos permite obtener información de las peticiones que hacemos a la base datos entre otros.


## Instalación de Clockwork

Ejecutamos el siguiente comando en la consola


```bash
    composer require itsgoingd/clockwork
```


## Usando Clockwork

Nos dirigimos a la siguiente [página](https://styde.net/clockwork-una-herramienta-para-depurar-tus-aplicaciones-de-php/ "Sitio web de Clockwork").


Descargamos la extensión de Clockwork según nuestro navegador.

![Extensión](images/cap-26.png)

Sobre el navegador seleccionamos la opción de inspeccionar y ahí podremos encontrar Clockwork

![Clockwork](images/cap-26-1.png)

## Solucionando el Problema

Mediante Clockwork podemos observar que estamos ejecutando varias consultas inccesarias a la base datos, con el objetivo de solucionar esto vamos a realizar las siguientes modificaciones:

En el archivo de rutas `web.php` modificamos la ruta que carga todos los posts

Vamos a cargar los posts con la categoría  mediante  `Post::with('category')`



```php
    Route::get('/', function () {
        return view('posts', ['posts' => Post::with('category')->all()]);
    });
```
