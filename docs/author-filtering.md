# Filtrando por Autor

A continuación vamos a filtrar los post por autor

## Método para filtrar

En el modelo Post se edita el método creado para filtrar agregando lo siguiente

Obtenemos los Posts que tengan un autor y que coincida su username


```php
        $query->when(
            $filters['author'] ?? false,
            fn ($query, $author) =>
            $query->whereHas(
                'author',
                fn ($query) =>
                $query->where('username', $author)
            )
        );

```


## Modificación componentes

Editamos los componentes post-card-product y post-featured dentro de la etiqueta `<h5>` agregamos un enlace que nos muestra los post filtrados por Autor

```html
<h5 class="font-bold">
 <a href="?/authors/{{ $post->author->username}}">{{ $post->author->name }}</a>
 </h5>

 ```


 ## Rutas

Corregimos el nombre de la vista que cambiamos anteriormente


```php
Route::get('authors/{author:username}', function (User $author) {
    return view('posts.index', [
        'posts' => $author->posts,
    ]);
});

```