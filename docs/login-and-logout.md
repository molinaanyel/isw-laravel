# Login and logout

En este capítulo se hará el login y el logout, dentro del register controller se debe crear

```php
User::create($attributes);

auth()->login($user);
 ```

 Con esto se tiene la validación de que el usuario efectivamente exista en la base de datos, además mediante un `` middleware `` se puede hacer la validación si es invitado, usuario registrado o si require registro, estos son segmentos de lógica que corren únicamente en determinadas situaciones.

 Posterior basado en cada escenario (si está registrado, si requiere registro, si es administrador o invitado) se crea un mensaje de bienvenida o de toma de acción.

 Para mantener hospedada la sesión del usuario se ingresa la siguiente línea

 ```php
artisan make:controller SessionController
 ```

 