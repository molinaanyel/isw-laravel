# Expand the Admin Layout

Dentro de la ruta  `` /components/layout.blade.php `` se va a cambiar el componente para que tenga la opción de una  `` Dropdown list `` para ello se ingresa el código:

```html
<x-dropdown>
    <x-slot name="trigger">
        <button class="text-xs font-bold uppercase">Welcome, {{ auth()->name }} </button>
    </x-slot>  
    <x-dropdown-item href="/admin/post/create"> New Post </xdropdown-item>  
<x-dropdown>
```

Se hace lo mismo para cada elemento de la lista.

Se debe ajustar el archivo de estilos y hacer el Drodown  `` Relative `` 