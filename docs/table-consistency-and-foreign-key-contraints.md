# Table consistency and foreign key contraints

El siguiente paso es crear una migración con la instrucción:

`` artisan make:model Comment -mfc ``

Esto creará el model requerido dentro de la base de datos para poder accederlo luego. De esta forma se pueden asociar los usuarios que leen el comentario del post.

```php
function up() {
    $chema::create('comments', function (Blueprint $table)) {
        $table->id();
        $table->unsignedBigIntener('post_id');
        $table->unsignedBigInteger('user_id');
        $table->text('body');
        $table->timestamps();
    }
};
 ```

 Esto permite guardar la información en la tabla, se utiliza el término `` unsigned `` para asegurarse de que el número sea siempre positivo.

 Luego se procede a migrar la base de datos:
 ``` php 
 artisan migrate 
 ```

 Esto modificará la `` Table Class `` agregando la información y si por ejemplo se borra un post asociado a un comentario, el comentario aún sigue existiendo, por lo cual con este proceso nos aseguramos de que cuando se borre el post, también desaparezcan los comentarios asociados. 

 Agregando la siguiente línea a `` function up() ``

 ```php
$table->foreign('post_id')->references('id')->on('posts')->cascadeOnDelete();
 ```
De esta manera nos aseguramos de poder eliminar realmente la información asociada a cualquier post una vez el post es eliminado.