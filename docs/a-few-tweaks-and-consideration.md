# Algunos ajustes y consideraciones

A continuación vamos a implementar un método para validar

## Creando método para validar

Se crea un método en el modelo Post, dicho método llama al método que creamos anteriormente ,`find($slug)`valida si existe un post, en caso contrario retorna una excepción con la función de Laravel  `throw new ModelNotFoundException();`

```php
    public static function findOrFail($slug)
    {
        $post =  static::find($slug);

        if (!$post) {
            throw new ModelNotFoundException();
        }

        return $post;
    }
 ```

 ## Implementación del método

 Modificamos el archivo `web.php` la ruta llamada `/posts`
 
 Sustituimos el método que teníamos anteriormente `find` por el que creamos `findOrFail`

 ```php
 Route::get('posts/{post}', function ($slug) {
    return view('post', ['post' => Post::findOrFail($slug)]);
});
 ```
