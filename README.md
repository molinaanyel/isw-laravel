# Curso Laravel From Scratch

## Capitulos

[Como una ruta carga una vista (05)](./docs/how-a-route-loads-a-view.md)

[Implementando diseño (06)](./docs/integrate-the-design.md)

[Creación y vinculo de una ruta con una vista (07)](./docs/make-a-route-and-link-to-it.md)

[Guardar un post como un archivo html (08)](./docs/store-blogspost-as-html-files.md)

[Restricciones de comodín de una ruta (09)](./docs/route-wildcard-constraints.md)

[Uso del almacenamiento caché(10)](./docs/use-caching-for-expensive-operations.md)

[Uso de funciones de la clase Filesystem  y modelos (11)](./docs/use-the-file-system-class-to-read-a-directory.md)

[Uso del paquete yaml-front-matter para leer metadatos (12)](./docs/find-a-composer-package-for-post-metadata.md)

[Almacenamiento de colecciones en caché y orden (13)](./docs/collection-sorting-caching-refresher.md)

[Conceptos básicos de blade (14)](./docs/blade-absolute-basics.md)

[Dos formas de usar layouts con blade (15)](./docs/blade-layouts-two-ways.md)


[Algunos ajustes y consideraciones (16)](./docs/a-few-tweaks-and-consideration.md)

[Configuración del ambiente para usar una base de datos (17)](./docs/enviroment-files-and-database-connections.md)

[Principios básicos de las migraciones (18)](./docs/migrations-the-absolute-basics.md)

[Eloquent y Active Record Patern (19)](./docs/eloquent-and-the-active-record-patern.md)

[Eloquent Model y Migraciones (20)](./docs/make-a-model-and-migration.md)

[Modificaciones con Eloquent Model y HTML Escaping (21)](./docs/eloquent-updates-and-HTML-escaping)

[Asignamiento Másivo (22)](./docs/3-ways-to-mitigate-mass-assignment-vulnerabilities.md)

[Route Model Binding (23)](./docs/route-model-binding.md)


[Eloquent y Relaciones (24)](./docs/your-first-eloquent-relationship.md)

[Mostrando todos los post asociados a una categoría (25)](./docs/show-all-post-associated-with-a-category.md)

[Usando Clockwork( 26)](./docs/show-all-post-associated-with-a-category.md)


[Generando datos con Seeders (27)](./docs/database-seeding-saves-time.md)


[Conceptos básicos de los factories (28)](./docs/turbo-boost-with-factories.md)

[Filtrando todos los Post de un autor (29)](./docs/view-all-posts-by-and-author.md)


[Cargando relaciones en un modelo (30)](./docs/eager-load-relationships-on-a-existing-model.md)


[Conviertiendo HTML y CSS a Blade (31)](./docs/convert-the-html-and-css-to-blade.md)

[Componentes de Blade y Css Grid (32)](./docs/blade-components-and-the-css-grids.md)

[Transformando la página Post (33)](./docs/convert-the-blog-post-page.md)

[Implementando un Dropdown con Javascript (34)](./docs/a-small-javacript-dropdown.md)

[Como extraer un componente blade (35)](./docs/how-to-extract-a-dropdown-blade-component.md)

[Algunos ajustes rápidos y limpieza (36)](./docs/quick-tweaks-and-clean-up.md)

[Búsqueda(manera no optimizada) (37)](./docs/search-the-messy-way.md)

[Búsqueda(manera optimizada) (38)](./docs/search-the-cleaner-way.md)

[Restricción de consultas avanzadas (39)](./docs/advanced-eloquent-query-constraints.md)

[Extraer un componente de categoría de una lista desplegable (40)](./docs/extract-a-category-dropdown.md)

[Filtrando por Autor (41)](./docs/author-filtering.md)

[Uniendo el Query de Categorías y Búsqueda(42)](./docs/merge-category-and-serach-queries.md)

[Arreglando un error de un Query (43)](./docs/fix-a-confusing-eloquent-query-bug.md)


[Paginación (44)](./docs/show-all-post-associated-with-a-category.md)

[Desarrollando una página de registro de usuarios (45)](./docs/build-a-register-user-page.md)

[Hasting de contraseñas (46)](./docs/automatic-password-hasting-with-mutators.md)

[Validación fallida y fecha de entrada anterior (47)](./docs/failed-validation-and-old-input-date.md)


[Mostrando un mensaje rápido de éxito (48)](./docs/show-all-post-associated-with-a-category.md)


[Login y Logout (49)](./docs/login-and-logout.md)

[Creando una página de login (50)](./docs/build-the-log-in-page.md)

[Laravel  Breeze (51)](./docs/laravel-breeze-quick-peek.md)

[Escribiendo el marcado para un comentario (52)](./docs/write-the-markup-for-a-post-comment.md)

[Coherencia de tablas y llaves foráneas (53)](./docs/table-consistency-and-foreign-key-contraints.md)

[Creando dinámicamente la sección de comentarios (54)](./docs/make-the-comments-section-dynamic.md)

[Diseñando el formulario de comentarios (55)](./docs/design-the-comment-form.md)

[Activando el formulario de comentarios (56)](./docs/activate-the-comment-form.md)

[Algunos cambios de limpieza en el código (57)](./docs/some-light-chapter-clean-up.md)

[Api Mailchimp (58)](./docs/mailchimp-api-tinkering.md)

[Hacer funcionar el form newsletter (59)](./docs/make-the-newsletter-form-work.md)

[Extraer un servicio newsletter (60)](./docs/extract-a-newsletter-service.md)


[Cofres de juguetes y contratos  (61)](./docs/toy-chests-and-contracts.md)

[Acceso limitado  (62)](./docs/limit-access-to-only-admins.md)

[Crear el form publico de Post (63)](./docs/create-the-publish-form.md)

[Validando y almacenando Posts (64)](./docs/validate-and-store-post-thumbnails.md)

[Extraer componentes especificos de un form (65)](./docs/extract-form-specific-components.md)

[Extender el layout del admin (66)](./docs/expand-the-admin-layout.md)

[Creación de un formulario para crear,editar y elminar (67)](./docs/create-a-form-to-edit-and-delete-posts.md)

[ Logica y almacenamiento en grupo (68)](./docs/group-and-store-validation-logic.md)

[Todo Acerca de Autorización (69)](./docs/all-about-authorization.md)

[Adios y siguientes pasos (70)](./docs/good-bye-and-next-steps.md)













































